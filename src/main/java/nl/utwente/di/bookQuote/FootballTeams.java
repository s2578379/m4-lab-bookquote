package nl.utwente.di.bookQuote;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(urlPatterns = {"./football"})
public class FootballTeams extends HttpServlet {
    private String title = "Football teams";
    private String docType = "<!DOCTYPE HTML>";

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        var a = req.getParameter("name");


        resp.setContentType("text/html");
        var out = resp.getWriter();
        out.println(docType
            + "<html>"
            + "<head>"
            + "<TITLE>" + title + "</TITLE>"
            + "<LINK REL=STYLESHEET HREF=\"styles.css\">"
            + "</head>"
            + "<body>"
            + "</body>"
            + "</html>"
        );
    }
}
