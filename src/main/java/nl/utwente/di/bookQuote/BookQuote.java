package nl.utwente.di.bookQuote;

import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;

/** Example of a Servlet that gets an ISBN number and returns the book price
 */
@WebServlet(
  description = "BookQuote Servlet",
  urlPatterns = {"/bookQuote"}
)
public class BookQuote extends HttpServlet {
 	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Quoter quoter;
	
  public void init() throws ServletException {
    quoter = new Quoter();
  }	
  public void doGet(
    HttpServletRequest request,
    HttpServletResponse response
  )
  throws ServletException, IOException
  {

    response.setContentType("text/html");
    PrintWriter out = response.getWriter();
    String docType = "<!DOCTYPE HTML>\n";
    String title = "Book Quote";
    String price = Double.toString(quoter.getBookPrice(request.getParameter("isbn")));
    String isbnString = request.getParameter("isbn");
    out.println(docType +
      "<HTML>\n" +
      "<HEAD><TITLE>" + title + "</TITLE>" +
      "<LINK REL=STYLESHEET HREF=\"styles.css\">" +
      "</HEAD>\n" +
      "<BODY BGCOLOR=\"#FDF5E6\">\n" +
      "<H1>" + title + "</H1>\n" +              
      "  <P>ISBN number: " + isbnString+ "\n" +
      "  <P>Price: " + price +
      "</BODY></HTML>");
    
  }
  

}
